

# TestReignNx

This project was generated using [Nx](https://nx.dev).

<p style="text-align: center;"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png" width="450"></p>

🔎 **Smart, Fast and Extensible Build System**

## Run project with docker

- `docker-compose build`
- `docker-compose up -d`

- [WebApp](http://localhost)
- [Api](http://localhost:3333)

## Run project local

- [WebApp] `npm run start:local:app`
- [Api] `npm run start:local:api`
- [WebApp-Api-Parallel] `npm run start:local:app:all`

## Run test-lint 
- `nx test api`
- `nx test posts`
- `nx lint api`
- `nx lint posts`
