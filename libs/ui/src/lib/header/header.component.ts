import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'test-reign-nx-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() title = 'Add title';
  @Input() subTitle = '';
  constructor() { }

  ngOnInit(): void {
  }

}
