import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { HttpBaseService } from './services';

@Module({
  imports: [HttpModule],
  controllers: [],
  providers: [HttpBaseService],
  exports: [HttpBaseService],
})
export class ApiCommonModule {}
