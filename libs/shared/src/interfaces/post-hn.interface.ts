export interface PostHN {
  _id?: string;
  postId: string;
  title: string;
  author: string;
  comment: string;
  url: string;
  createdAt: string;
  status?: boolean;
  __v?: number;
}

