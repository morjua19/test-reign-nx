import { MethodHttp } from '../enums';

export interface ConfigUrlService {
  url: string;
  method: MethodHttp;
  headerToken?: string;
  hash?: boolean;
}

export interface ConfigEnv {
  env: string;
  requireEncrypted: boolean;
  publicKey: string;
  privateKey: string;
  secretKey: string;
}
