import { Module } from '@nestjs/common';
import * as Joi from '@hapi/joi';

import { AppService } from './app.service';
import { PostService } from './services/post/post.service';
import { PostController } from './controllers/post/post.controller';
import { ApiCommonModule } from '@test-reign-nx/api-common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { PostSchema, Post } from './schemas/post.schema';
import { PostApiService } from './services/post-api/post-api.service';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    ApiCommonModule,
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: `.${process.env.NODE_ENV || 'local'}.env`,
      validationSchema: Joi.object({
        NODE_ENV: Joi.string().valid('local', 'production').default('local'),
      }),
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        uri: configService.get<string>('MONGODB_URL'),
      }),
      inject: [ConfigService],
    }),
    MongooseModule.forFeature([{ name: Post.name, schema: PostSchema }]),
    ScheduleModule.forRoot(),
  ],
  controllers: [PostController],
  providers: [AppService, PostService, PostApiService],
})
export class AppModule {}
