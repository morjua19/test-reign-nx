import { Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { PostService } from '../../services/post/post.service';

@Controller('post')
export class PostController {
  constructor(protected readonly postService: PostService) {
  }

  @Post()
  @HttpCode(HttpStatus.OK)
  async getPosts() {
    return this.postService.getList();
  }

  // @Post()
  // @HttpCode(HttpStatus.OK)
  // save() {
  //   return this.postService.saveBatch()
  // }
}
