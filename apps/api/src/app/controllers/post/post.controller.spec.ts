import { Test, TestingModule } from '@nestjs/testing';
import { PostController } from './post.controller';
import { PostService } from '../../services/post/post.service';
import { PostApiService } from '../../services/post-api/post-api.service';
import { getModelToken } from '@nestjs/mongoose';
import { HttpBaseService } from '@test-reign-nx/api-common';
import { HttpModule } from '@nestjs/axios';


const mockPostsResponse = [
  {
    _id: '624731e9a475e16addb02778',
    postId: '30879760',
    title: 'Ask HN: Who wants to be hired? (April 2022)',
    author: 'keraf',
    comment: 'Full stack dev with +10 years experience.',
    url: null,
    status: true,
    createdAt: '2022-04-01T16:10:59.000Z',
    __v: 0
  }
]

describe('PostController', () => {
  let controller: PostController;
  const postService = { getList: () => mockPostsResponse }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostService,
        PostApiService,
        { provide: getModelToken('Post'), useValue: {}},
        HttpBaseService,
      ],
      imports: [
        HttpModule
      ],
      controllers: [PostController],
    })
      .overrideProvider(PostService)
      .useValue(postService)
      .compile();

    controller = module.get<PostController>(PostController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getPosts()', () => {
    it('should call postService', async () => {
      const result = mockPostsResponse;
      const spyService = jest.spyOn(postService, 'getList').mockImplementation(() => result);
      const resp = await controller.getPosts();
      expect(resp).toBe(result);
      expect(spyService).toHaveBeenCalled();
    });
  })


});
