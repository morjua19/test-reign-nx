import { CallHandler, ExecutionContext, HttpStatus, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GlobalResponse } from '@test-reign-nx/shared';

@Injectable()
export class TransformInterceptor<T> implements NestInterceptor<T, GlobalResponse<T>> {
  createResponse(data: T): GlobalResponse<T> {
    return {
      status: {
        code: HttpStatus.OK,
        message: 'ok',
      },
      data,
    };
  }
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(map(this.createResponse));
  }
}
