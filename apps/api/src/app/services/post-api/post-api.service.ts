import { Injectable } from '@nestjs/common';
import { PostApiResponse } from '../../interfaces/post-api.interface';
import { ConfigUrlService, MethodHttp } from '@test-reign-nx/shared';
import { HttpBaseService } from '@test-reign-nx/api-common';

@Injectable()
export class PostApiService {
  constructor(
    private httpBase: HttpBaseService,
  ) {
  }

  getPosts(): Promise<PostApiResponse> {
    const payload: ConfigUrlService = {
      url: 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
      method: MethodHttp.GET
    };
    const header = {};
    return this.httpBase.createRequest<PostApiResponse>(payload, header)
  }
}
