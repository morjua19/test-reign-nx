import { Test, TestingModule } from '@nestjs/testing';
import { PostApiService } from './post-api.service';
import { HttpBaseService } from '@test-reign-nx/api-common';
import { HttpModule } from '@nestjs/axios';

describe('PostApiService', () => {
  let service: PostApiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostApiService,
        HttpBaseService
      ],
      imports: [
        HttpModule
      ]
    }).compile();

    service = module.get<PostApiService>(PostApiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
