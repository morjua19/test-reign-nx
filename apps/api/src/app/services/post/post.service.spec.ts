import { Test, TestingModule } from '@nestjs/testing';
import { PostService } from './post.service';
import { getModelToken } from '@nestjs/mongoose';
import { PostApiService } from '../post-api/post-api.service';
import { HttpBaseService } from '@test-reign-nx/api-common';
import { HttpModule } from '@nestjs/axios';

describe('PostService', () => {
  let service: PostService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostService,
        { provide: getModelToken('Post'), useValue: {}},
        PostApiService,
        HttpBaseService,
      ],
      imports: [
        HttpModule
      ],
    }).compile();

    service = module.get<PostService>(PostService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
