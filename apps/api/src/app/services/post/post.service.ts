import { Injectable, Logger } from '@nestjs/common';
import { Model } from 'mongoose';
import { Post, PostDocument } from '../../schemas/post.schema';
import { InjectModel } from '@nestjs/mongoose';
import { PostApiService } from '../post-api/post-api.service';
import { Cron } from '@nestjs/schedule';
import { PostHN } from '@test-reign-nx/shared';

@Injectable()
export class PostService {
  private readonly logger = new Logger(PostService.name);

  constructor(
    private postApiService: PostApiService,
    @InjectModel(Post.name)
    private postDocumentModel: Model<PostDocument>,
  ) {
  }

  async getAll(): Promise<PostHN[]> {
    return this.postDocumentModel.find();
  }

  async getList(): Promise<PostHN[]> {
    return this.postDocumentModel.find({
      title: {$ne: null}
    });
  }

  getByStoryId(postId: string) {
    return this.postDocumentModel.findOne({
      postId,
    });
  }

  async saveBatch(): Promise<void> {
    const posts = await this.postApiService.getPosts();

    for (const item of posts.hits) {
      const postExits = await this.getByStoryId(item.objectID);
      if (!postExits) {
        const post: PostHN = {
          postId: item.objectID,
          author: item.author,
          comment: item.comment_text,
          title: item.story_title || item.title,
          url: item.story_url || item.url,
          createdAt: item.created_at,
        }
        await this.postDocumentModel.create(post);
      }
    }
  }

  @Cron('0 10 * * * *')
  async savePostCron() {
    await this.saveBatch();
    this.logger.debug('Called saveBatch');
  }
}
