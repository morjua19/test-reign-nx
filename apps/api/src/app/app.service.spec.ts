import { Test } from '@nestjs/testing';

import { AppService } from './app.service';
import { PostService } from './services/post/post.service';
import { PostApiService } from './services/post-api/post-api.service';
import { getModelToken } from '@nestjs/mongoose';
import { HttpModule } from '@nestjs/axios';
import { HttpBaseService } from '@test-reign-nx/api-common';
import { PostHN } from '@test-reign-nx/shared';

const mockPostsResponse: PostHN[] = [
  {
    _id: '624731e9a475e16addb02778',
    postId: '30879760',
    title: 'Ask HN: Who wants to be hired? (April 2022)',
    author: 'keraf',
    comment: 'Full stack dev with +10 years experience.',
    url: null,
    status: true,
    createdAt: '2022-04-01T16:10:59.000Z',
    __v: 0
  }
]

describe('AppService', () => {
  let service: AppService;
  const postService = {
    getAll: () => [],
    saveBatch: () => undefined
  }

  beforeAll(async () => {
    const app = await Test.createTestingModule({
      providers: [
        AppService,
        PostService,
        PostApiService,
        { provide: getModelToken('Post'), useValue: {}},
        HttpBaseService
      ],
      imports: [
        HttpModule
      ],
    })
      .overrideProvider(PostService)
      .useValue(postService)
      .compile();

    service = app.get<AppService>(AppService);
  });

  describe('initApp', () => {
    it('should be called [saveBatch]', async () => {
      const spyService = jest.spyOn(postService, 'saveBatch').mockImplementation(() => Promise.resolve());
      await service.initApp();
      expect(spyService).toHaveBeenCalled();
    });

    it('should not be [saveBatch]', async () => {
      const postService2 = {
        getAll: () => mockPostsResponse,
        saveBatch: () => undefined
      }
      const spyService = jest.spyOn(postService2, 'saveBatch').mockImplementation(() => Promise.resolve());
      await service.initApp();
      expect(spyService).not.toHaveBeenCalled();
    });
  });

  // describe('onModuleInit', () => {
  //   it('should be -', async () => {});
  // });
});
