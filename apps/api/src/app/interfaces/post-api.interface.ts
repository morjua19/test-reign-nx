export interface PostApiResponse {
  hits: Hit[];
}

export interface Hit {
  created_at: string;
  title: string;
  url: string;
  author: string;
  points: string | number;
  story_text: string;
  comment_text: string;
  num_comments: string | number;
  story_id: number;
  story_title: string;
  story_url: string;
  parent_id: number;
  created_at_i: number;
  _tags: string[];
  objectID: string;
  _highlightResult: HighlightResult;
}

export interface HighlightResult {
  author: Author;
  comment_text: Author;
  story_title: Author;
}

export interface Author {
  value: string;
  matchLevel: string;
  matchedWords: string[];
  fullyHighlighted?: boolean;
}
