import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PostDocument = Post & Document;

@Schema()
export class Post {
  @Prop({ required: true, unique: true })
  postId: string;

  @Prop({ required: false })
  title: string;

  @Prop({ required: true })
  author: string;

  @Prop({ required: true })
  comment: string;

  @Prop({ required: false })
  url: string;

  @Prop({ default: true })
  status: boolean;

  @Prop({ required: true })
  createdAt: string;
}

export const PostSchema = SchemaFactory.createForClass(Post);
