import { Injectable, OnModuleInit } from '@nestjs/common';
import { PostService } from './services/post/post.service';

@Injectable()
export class AppService implements OnModuleInit {
  constructor(private postService: PostService) {
  }

  async initApp(): Promise<void> {
    const posts = await this.postService.getAll();
    if (posts.length <= 0) {
      await this.postService.saveBatch();
      console.log('Saved posts [INIT] - Successfully')
    }
  }

  async onModuleInit(): Promise<void> {
    await this.initApp();
    console.log(`The module [INIT] has been initialized.`);
  }
}
