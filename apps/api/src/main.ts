/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { tableLogRoutes } from './app/helpers/utils';
import { TransformInterceptor } from './app/interceptors/transform.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const env = process.env.NODE_ENV;
  const globalPrefix = 'api';
  const name = 'api-posts-ms';

  app.setGlobalPrefix(globalPrefix);
  const port = process.env.PORT || 3333;

  app.useGlobalInterceptors(new TransformInterceptor());
  app.enableCors();

  await app.listen(port);
  Logger.log(
    `🚀 Application is running on: http://localhost:${port}/${globalPrefix}`
  );
  console.log(`\nService listening at http://localhost:${port}`);
  console.log(
    `\x1b[33mstarting  the microservice [ ${name} ]. at ${Date().toString()}`,
  );
  console.log(`\x1b[32mrunning environment NODE_ENV=${process.env.NODE_ENV}`);

  if (env !== 'production') {
    tableLogRoutes(app.getHttpServer(), globalPrefix);
  }
}

bootstrap();
