import { NgModule } from '@angular/core';
import { IndexComponent } from './index/index.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { PostsComponent } from '../../features/posts/posts.component';
import { AlertComponent } from '../../components/alert/alert.component';

@NgModule({
  declarations: [
    IndexComponent,
    PostsComponent,
    AlertComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: IndexComponent }]),
  ]
})
export class MainModule { }
