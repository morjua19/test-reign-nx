import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { GlobalResponse } from '@test-reign-nx/shared';
import { Observable, throwError } from 'rxjs';
import { catchError, timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient) { }

  public headers() {
    return {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };
  }



  public msCall<T = any>(body: any, path: string): Observable<GlobalResponse<T>> {
    const httpMicroServiceCall = {
      headers: new HttpHeaders(this.headers())
    };
    const serviceUrl = path;

    return this.http.post<T>(serviceUrl, body, httpMicroServiceCall)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse): Observable<any> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status || 500}, ` +
        `Body was: ${error.error || 'Internal Server Error'}`);
    }
    // return an observable with a user-facing error message
    return throwError(error.status);
  }
}
