import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  show$ = new Subject<boolean>()

  show(): void {
    this.show$.next(true)
  }

  hide(): void {
    this.show$.next(false)
  }
}
