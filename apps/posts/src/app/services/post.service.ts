import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { GlobalResponse, PostHN } from '@test-reign-nx/shared';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private apiService: ApiService) { }


  getList(): Observable<GlobalResponse<PostHN[]>> {
    const path = 'http://localhost:3333/api/post';
    return this.apiService.msCall({ }, path);
  }
}
