import { Component } from '@angular/core';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'test-reign-nx-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent {
  show$ = this.alertService.show$;

  constructor(private alertService: AlertService) { }

  close() {
    this.alertService.hide()
  }
}
