import { Component, OnInit } from '@angular/core';
import { PostHN } from '@test-reign-nx/shared';
import { PostService } from '../../services/post.service';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'test-reign-nx-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  posts: PostHN[] = [];

  constructor(private postService: PostService, private alertService: AlertService) {
  }

  ngOnInit(): void {
    this.handlePosts();
  }

  handlePosts() {
    this.postService.getList().subscribe((response) => {
      this.posts = response.data
    });
  }

  goToUrlPost(url:string) {
    if (!url) {
      this.alertService.show();
      return;
    }
    window.open(url, '_blank');
  }

  removeItem(event: any, id: string) {
    event.stopImmediatePropagation();

    this.posts = this.posts.filter(item => (item.postId !== id))
  }

}
